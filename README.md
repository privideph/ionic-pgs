
## App description

Test app for ionic framework: list of users, user edit form, simple authorization

## App wireframes

![list view](https://bytebucket.org/privideph/ionic-pgs/raw/1c0285257ef50554fa8f639f7985b48a4d29ae97/users.png "User list view")
![edit view](https://bytebucket.org/privideph/ionic-pgs/raw/1c0285257ef50554fa8f639f7985b48a4d29ae97/users-edit.png "User edit view")

## Run in Ionic View:

* Create account on https://apps.ionic.io
* Download this app https://itunes.apple.com/us/app/ionic-view/id849930087?mt=8&ign-mpt=uo%3D4
* Open Ionic App by ID: f974c75c
* login by entering ANY credentials

