
angular.module('ionic-pgs.controllers')
    .controller('UsersEditMainCtrl', function($scope, user, $state, $ionicNavBarDelegate, UsersService, Toaster) {
console.log('wchodze tu?');
		var vm = this;
		this.emailRegex = /^[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$/;
		this.getPhoto = getPhoto;
		this.title = user._id ?
			'Edit user: ' + user.name.first + ' ' + user.name.last :
			'Add user';
        $scope.goToList = goToList;
        $scope.save = save;
        $scope.user = user;
		$scope.editedUser = angular.copy($scope.user);
console.log(this.title);
        function save() {

            var saveMsg = 'User saved!';

            if (vm.userForm.$valid) {
                if (!angular.equals($scope.editedUser, $scope.user)) {
                    UsersService.saveUser($scope.editedUser).then(function(){
                        Toaster.show(saveMsg);
						$state.go($state.current, {}, {reload: true});
                    }, function(reason){
                        Toaster.show('Error occured: ' + reason);
                    });
                }
                else
                {
                    // no changes, just inform of successful save
                    Toaster.show(saveMsg);
                }
            }
        }

        function goToList() {
            try
            {
                $ionicNavBarDelegate.back();
            }
            catch(Error)
            {
                $state.go('app.users', {}, {replace: true});
            }
        }

        function getPhoto() {
			console.log('photo', $scope.editedUser);
            return $scope.editedUser.picture || 'http://lorempixel.com/200/200/abstract/1';
        }
    });
