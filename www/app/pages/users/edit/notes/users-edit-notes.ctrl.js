
angular.module('ionic-pgs.controllers')
	.controller('UsersEditNotesCtrl', function($scope, user, UsersService, $ionicModal, $timeout) {
		var vm = this;

		user.notes = user.notes || [];

		$scope.addNote = addNote;
		$scope.deleteNote = deleteNote;
		$scope.note = {};
		$scope.notes = [];
		$scope.modalTitle = 'Add note';
		$scope.saveNote = saveNote;

		initModal();
		loadNotes();

		$scope.$on('$destroy', onScopeDestroy);
		$scope.$on('modal.hidden', onModalHidden);

		///////////////////////////////////////////

		function addNote() {
			$scope.modal.show();
		}

		function saveNote(note) {
			if(note.content || note.title)
			{
				note.time = (new Date()).getTime();
				user.notes.push(note);
				UsersService.saveUser(user).then(function(){
					loadNotes();
					$scope.modal.hide();
				});
			}
			else
			{
				$scope.modal.hide();
			}
		}

		function deleteNote(idx) {
			var tmp = angular.copy($scope.notes);
			tmp.splice(idx, 1);
			user.notes = tmp;
			UsersService.saveUser(user).then(function(){
				$scope.notes = [];
				$timeout(function(){
					loadNotes();
				});
			});
		}

		function initModal() {
			$ionicModal.fromTemplateUrl('app/pages/users/edit/notes/users-edit-notes-form.html', {
				scope: $scope,
				focusFirstInput: true,
				animation: 'slide-in-up'
			}).then(function(modal) {
				$scope.modal = modal;
			}, function(){
			});
		}

		function loadNotes() {
			$scope.notes = angular.copy(user.notes);
		}

		function onScopeDestroy() {
			$scope.modal.remove();
		}

		function onModalHidden() {
			$scope.note = {};
		}
	});
