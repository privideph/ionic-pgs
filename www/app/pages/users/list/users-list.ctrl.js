angular.module('ionic-pgs.controllers')
    .controller('UsersListCtrl', function($scope, UsersService, Toaster) {
        $scope.deleteUser = deleteUser;
        $scope.users = [];
        $scope.usersFilter = '';

        getUsers();

        /////

        function deleteUser(id) {
            UsersService.deleteUser(id).then(function(){
                getUsers();
                Toaster.show('User deleted');
            });
        }

        function getUsers() {
            UsersService.getUsers().then(function(data){
                $scope.users = data;
                return $scope.users;
            });
        }
    });
