'use strict';

angular.module('ionic-pgs.controllers')
    .controller('LoginCtrl', function ($scope, $state, AuthService, Toaster) {
        $scope.credentials = {};
        $scope.login = login;

        function login(credentials) {
            if($scope.vm.loginForm.$invalid)
            {
                return;
            }

            return AuthService.login(credentials).then(onLogin, onError);

            function onLogin() {
                $state.go('app.users');
            }

            function onError(reason) {
                Toaster.show('Login error: '+reason);
                return reason;
            }
        }

    });
