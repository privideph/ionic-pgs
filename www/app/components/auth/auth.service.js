"use strict";

angular
    .module('ionic-pgs.components')
    .factory('AuthService', function ($q, $window) {
        var defaultUser = {
            userName: null
        };
        var storageKey = 'currentUser';

        patchLocalStorage();

        return {
            login: login,
            logout: logout,
            isAuthenticated: isAuthenticated
        };

        // this is not pretty, but it's here just for demo.
        function patchLocalStorage(){
            if(!('localStorage' in $window))
            {
                $window.localStorage = {};
            }
        }

        function login(credentials) {
            // wrapping with promise to easy switch to $http implementation if needed
            var deferred = $q.defer();
            var currentUser = angular.copy(defaultUser);
            if (credentials.userName && credentials.userPassword) {
                currentUser.userName = credentials.userName;
                saveUserData(currentUser);
                deferred.resolve(currentUser);
            }
            else {
                deferred.reject('Invalid credentials');
            }

            return deferred.promise;
        }

        function logout() {
            var deferred = $q.defer();
            var currentUser = angular.copy(defaultUser);
            saveUserData(currentUser);
            deferred.resolve(currentUser);
            return deferred.promise;
        }

        function saveUserData(currentUser){
            $window.localStorage[storageKey] = JSON.stringify(currentUser);
        }

        function isAuthenticated() {
            return !!getCurrentUser().userName;
        }

        function getCurrentUser() {
            return JSON.parse($window.localStorage[storageKey] || "") || angular.copy(defaultUser);
        }

    })
;
