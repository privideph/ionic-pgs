
angular.module('ionic-pgs.components')
    .controller('MenuCtrl', function($scope, AuthService, $state) {
    $scope.logout = logout;

    function logout(){
        return AuthService.logout().then(onLogout);
    }

    function onLogout(){
        $state.go('login');
    }





});