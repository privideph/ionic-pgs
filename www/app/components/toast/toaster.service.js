// Requires the Toast plugin: https://github.com/EddyVerbruggen/Toast-PhoneGap-Plugin
// And Ionic Framework: http://ionicframework.com
// ngCordova is used here, but easily removed: http://ngcordova.com/

// When running in Cordova, show the native toast. Outside of Cordova, show an Ionic Popup for the same period of time.
// Uses the API for the Toast plugin - message, duration, position.
// Differences are that: Ionic Popup ignores position, and doesn't allow doing anything while it shows.
angular.module('ionic-pgs.components')
    .factory('Toaster', function($rootScope, $timeout, $ionicPopup, $cordovaToast) {
    return {
        show: function (message, duration, position) {
            message = message || "There was a problem...";
            duration = duration || 'short';
            position = position || 'top';

            try
            {
                $cordovaToast.show(message, duration, position);
            }
            catch(Error)
            {
                if (duration == 'short') {
                    duration = 1500;
                }
                else {
                    duration = 3000;
                }

                var myPopup = $ionicPopup.show({
                    title: message,
                    template: "",
                    scope: $rootScope,
                    buttons: []
                });

                $timeout(function() {
                    myPopup.close();
                }, duration);
            }
        }
    };
})
