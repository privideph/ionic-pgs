"use strict";

angular
    .module('ionic-pgs.components')
    .factory('UsersService', function (StorageService) {
        return {
            getUsers: getUsers,
            getUser: getUser,
            saveUser: saveUser,
            deleteUser: deleteUser,
            newUser: newUser
        };

        /////////////

        function getUsers() {
            return StorageService.read('/users').then(function (result) {
                return result || getDummy();
            });
        }

        function getUser(id) {
            return getUsers().then(function (result) {
                result = result || [];

                // .filter().pop() rather than .find() for better browser coverage
                return result.filter(function (user) {
                    return user._id === id;
                }).pop();

            });
        }

        function saveUser(user) {
            return getUsers().then(function (users) {
                var idx = -1;

                if (user._id) {
                    idx = findIndex(users, user._id);
                }
                else {
                    user._id = createUUID();
                }

                if (idx >= 0) {
                    users.splice(idx, 1, user);
                }
                else {
                    users.push(user);
                }

                return StorageService.write('/users', users);
            });

        }

        function deleteUser(user) {
            var id = angular.isObject(user) ? user._id : user;

            return getUsers().then(function (users) {
                var idx = findIndex(users, id);
                if (idx >= 0) {
                    users.splice(idx, 1);
                    StorageService.write('/users', users);
                }
                else {
                    return true;
                }
            });
        }

        function newUser() {
            return {
                "_id": null,
                "picture": "http://lorempixel.com/200/200/nature/1",
                "name": {
                    "first": "",
                    "last": ""
                },
                "email": "",
                "description": ""
            }
        }

        function createUUID() {
            // http://www.ietf.org/rfc/rfc4122.txt
            var s = [];
            var hexDigits = "0123456789abcdef";
            for (var i = 0; i < 36; i++) {
                s[i] = hexDigits.substr(Math.floor(Math.random() * 0x10), 1);
            }
            s[14] = "4";  // bits 12-15 of the time_hi_and_version field to 0010
            s[19] = hexDigits.substr((s[19] & 0x3) | 0x8, 1);  // bits 6-7 of the clock_seq_hi_and_reserved to 01
            s[8] = s[13] = s[18] = s[23] = "-";

            return s.join("");

        }

        function findIndex(users, id) {
            var idx = -1;

            // find user's index
            angular.forEach(users, function (obj, i) {
                if (obj._id === id) {
                    idx = i;
                    return false;
                }
            });
            return idx;

        }

        // kids, don't do this at home!
        function getDummy() {
            return [
                {
                    "_id": "54be58e3c31745227fa02b0f",
                    "picture": "http://lorempixel.com/200/200/sports/1",
                    "name": {
                        "first": "Daniel",
                        "last": "McBruce"
                    },
                    "email": "mcdaniel.bruce@mail.tld",
                    "description": "Excepteur dolor tempor aute in et et. Qui dolor esse mollit ea laboris ea."
                },
                {
                    "_id": "54be58e38bbe00a27616ba39",
                    "picture": "http://lorempixel.com/200/200/sports/2",
                    "name": {
                        "first": "Duran",
                        "last": "Hart"
                    },
                    "email": "duran.hart@mail.tld",
                    "description": "In consequat cupidatat duis pariatur consectetur adipisicing ex id labore. Duis aliqua non voluptate non enim proident nostrud pariatur aliqua Lorem."
                },
                {
                    "_id": "54be58e3a3ea6e85f578dd9b",
                    "picture": "http://lorempixel.com/200/200/sports/3",
                    "name": {
                        "first": "Nadine",
                        "last": "Farmer"
                    },
                    "email": "nadine.farmer@mail.tld",
                    "description": "Et consequat amet dolore ad exercitation dolor nulla aliquip ad ex reprehenderit minim commodo. Anim et dolore sit excepteur minim sit commodo veniam cillum duis reprehenderit commodo."
                },
                {
                    "_id": "54be58e3d98d3f616ca01434",
                    "picture": "http://lorempixel.com/200/200/sports/4",
                    "name": {
                        "first": "Cruz",
                        "last": "Wilkerson"
                    },
                    "email": "cruz.wilkerson@mail.tld",
                    "description": "Consequat in mollit elit consequat esse ipsum ipsum ad nostrud veniam sit. Sint labore velit duis Lorem culpa et proident mollit ad minim."
                },
                {
                    "_id": "54be58e3e804de3532ae2d65",
                    "picture": "http://lorempixel.com/200/200/sports/5",
                    "name": {
                        "first": "Spencer",
                        "last": "Mcknight"
                    },
                    "email": "spencer.mcknight@mail.tld",
                    "description": "Amet cupidatat laboris dolore ex deserunt tempor proident veniam magna veniam. Labore dolore eu in amet minim velit ad veniam ad enim minim."
                },
                {
                    "_id": "54be58e3918c214d531da8b1",
                    "picture": "http://lorempixel.com/200/200/sports/6",
                    "name": {
                        "first": "Sears",
                        "last": "Coffey"
                    },
                    "email": "sears.coffey@mail.tld",
                    "description": "Incididunt cillum eu cupidatat esse fugiat deserunt deserunt culpa adipisicing ea sit deserunt. Nisi id nisi ex exercitation id."
                },
                {
                    "_id": "54be58e369c702299bb77411",
                    "picture": "http://lorempixel.com/200/200/sports/7",
                    "name": {
                        "first": "Kelsey",
                        "last": "Beach"
                    },
                    "email": "kelsey.beach@mail.tld",
                    "description": "Anim cupidatat excepteur aliquip cillum qui dolore pariatur minim nostrud anim amet reprehenderit ex laborum. Aliqua est fugiat quis aliqua consectetur proident labore adipisicing consequat proident deserunt adipisicing."
                },
                {
                    "_id": "54be58e35466d9c848df605a",
                    "picture": "http://lorempixel.com/200/200/sports/8",
                    "name": {
                        "first": "Mathews",
                        "last": "Myers"
                    },
                    "email": "mathews.myers@mail.tld",
                    "description": "Aliquip voluptate duis voluptate irure magna commodo esse non elit aliquip aliquip. Aliqua labore est ex deserunt qui nulla culpa sunt nostrud eu laborum sint."
                },
                {
                    "_id": "54be58e3c871917f5f395881",
                    "picture": "http://lorempixel.com/200/200/sports/9",
                    "name": {
                        "first": "Finley",
                        "last": "Lewis"
                    },
                    "email": "finley.lewis@mail.tld",
                    "description": "Nisi minim est dolore cupidatat in deserunt magna quis. Dolor minim eu tempor labore veniam nisi."
                },
                {
                    "_id": "54be58e3841765e90d4c7785",
                    "picture": "http://lorempixel.com/200/200/nature/2",
                    "name": {
                        "first": "Snow",
                        "last": "Kennedy"
                    },
                    "email": "snow.kennedy@mail.tld",
                    "description": "Sunt laboris eu excepteur in ut ex. Voluptate anim in irure elit ullamco ea laboris dolore ipsum elit et ea officia."
                },
                {
                    "_id": "54be58e3a765420f11d47d2b",
                    "picture": "http://lorempixel.com/200/200/nature/3",
                    "name": {
                        "first": "Leonor",
                        "last": "Underwood"
                    },
                    "email": "leonor.underwood@mail.tld",
                    "description": "Commodo enim consectetur sunt commodo magna deserunt voluptate adipisicing ipsum. Est nulla magna id nostrud voluptate exercitation occaecat veniam exercitation adipisicing reprehenderit ad aliquip incididunt."
                },
                {
                    "_id": "54be58e3b00bd590abedc950",
                    "picture": "http://lorempixel.com/200/200/nature/4",
                    "name": {
                        "first": "Diann",
                        "last": "Steele"
                    },
                    "email": "diann.steele@mail.tld",
                    "description": "Voluptate ut deserunt ut magna amet laboris. Lorem excepteur cillum mollit laborum id ut in proident."
                },
                {
                    "_id": "54be58e35b49e7c8f08a42a0",
                    "picture": "http://lorempixel.com/200/200/nature/5",
                    "name": {
                        "first": "Britney",
                        "last": "Mcclain"
                    },
                    "email": "britney.mcclain@mail.tld",
                    "description": "Proident ipsum laborum dolor velit aliqua. Nisi sunt consectetur ad ex veniam consequat culpa."
                },
                {
                    "_id": "54be58e38707b4ed0eebb38f",
                    "picture": "http://lorempixel.com/200/200/nature/6",
                    "name": {
                        "first": "Tia",
                        "last": "Lloyd"
                    },
                    "email": "tia.lloyd@mail.tld",
                    "description": "Duis laborum anim est consequat excepteur sit est et eu reprehenderit nisi voluptate. Et excepteur irure dolor nisi nisi eu quis ea labore dolore elit amet."
                },
                {
                    "_id": "54be58e3c3074644b795698a",
                    "picture": "http://lorempixel.com/200/200/nature/7",
                    "name": {
                        "first": "Bowers",
                        "last": "Miles"
                    },
                    "email": "bowers.miles@mail.tld",
                    "description": "Lorem ullamco proident deserunt ea excepteur irure voluptate qui aliqua irure. Eu aliqua dolore reprehenderit voluptate ipsum laborum in."
                },
                {
                    "_id": "54be58e325eac13e0a612605",
                    "picture": "http://lorempixel.com/200/200/nature/8",
                    "name": {
                        "first": "Blackwell",
                        "last": "Fleming"
                    },
                    "email": "blackwell.fleming@mail.tld",
                    "description": "Aute excepteur reprehenderit velit magna nostrud sit. Sunt officia sunt sunt tempor excepteur amet consectetur anim enim sit sint ut commodo."
                },
                {
                    "_id": "54be58e30241c2b3714c131e",
                    "picture": "http://lorempixel.com/200/200/nature/9",
                    "name": {
                        "first": "Melton",
                        "last": "King"
                    },
                    "email": "melton.king@mail.tld",
                    "description": "Anim proident aliqua consequat ex. Cillum laboris adipisicing aute tempor adipisicing dolore."
                },
                {
                    "_id": "54be58e306a56cd76e0c1ba1",
                    "picture": "http://lorempixel.com/200/200/nightlife/1",
                    "name": {
                        "first": "Shepherd",
                        "last": "Acosta"
                    },
                    "email": "shepherd.acosta@mail.tld",
                    "description": "Ad ea consequat duis nostrud ea do magna sint cupidatat voluptate quis aliqua. Ullamco mollit dolor consectetur officia laboris ut incididunt laborum et veniam nostrud laboris."
                },
                {
                    "_id": "54be58e3e6631baf94141fa0",
                    "picture": "http://lorempixel.com/200/200/nightlife/2",
                    "name": {
                        "first": "Beverly",
                        "last": "Dejesus"
                    },
                    "email": "beverly.dejesus@mail.tld",
                    "description": "Veniam labore commodo dolore velit voluptate proident consectetur nisi amet laboris. Laborum nostrud officia enim tempor pariatur minim dolor."
                },
                {
                    "_id": "54be58e3b27b606c4c93caf8",
                    "picture": "http://lorempixel.com/200/200/nightlife/3",
                    "name": {
                        "first": "Chan",
                        "last": "Valenzuela"
                    },
                    "email": "chan.valenzuela@mail.tld",
                    "description": "Labore mollit non do ex tempor do laboris tempor cillum laboris aliquip consequat exercitation nostrud. Cupidatat sunt officia cillum excepteur veniam laboris excepteur dolor."
                },
                {
                    "_id": "54be58e3336ab7244694659a",
                    "picture": "http://lorempixel.com/200/200/nightlife/4",
                    "name": {
                        "first": "Jocelyn",
                        "last": "Fitzgerald"
                    },
                    "email": "jocelyn.fitzgerald@mail.tld",
                    "description": "Consequat veniam consectetur dolore quis proident. Consequat elit nostrud voluptate adipisicing minim."
                },
                {
                    "_id": "54be58e3237b9adad870aa0d",
                    "picture": "http://lorempixel.com/200/200/nightlife/5",
                    "name": {
                        "first": "Keller",
                        "last": "Lowe"
                    },
                    "email": "keller.lowe@mail.tld",
                    "description": "Enim nisi laborum sunt sit. Ad Lorem tempor magna mollit eu dolore labore aliquip minim voluptate."
                },
                {
                    "_id": "54be58e36e69493c8dc56150",
                    "picture": "http://lorempixel.com/200/200/nightlife/6",
                    "name": {
                        "first": "Gabriela",
                        "last": "Leon"
                    },
                    "email": "gabriela.leon@mail.tld",
                    "description": "Non esse labore ut aliquip non do duis exercitation commodo. Aliquip aliqua officia sint do sint incididunt incididunt."
                }
            ];
        }

    });
