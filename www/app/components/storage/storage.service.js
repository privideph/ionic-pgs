"use strict";

angular
    .module('ionic-pgs.components')
    // wrapping localStorage with promises to be compatible with $http
    .factory('StorageService', function ($q, $window) {

        var rejectReason = 'Local storage not supported';

        var service = {
            write: write,
            read: read,
            remove: remove
        };

        return service;

        /////////////////

        function isSupported() {
            return 'localStorage' in $window;
        }

        function write(key, value) {
            var deferred = $q.defer();
            if (isSupported()) {
                $window.localStorage[key] = JSON.stringify(value);
                deferred.resolve(true);
            }
            else {
                deferred.reject(rejectReason)
            }
            return deferred.promise;
        }

        function read(key) {
            var deferred = $q.defer();
            if (isSupported()) {
                deferred.resolve(JSON.parse($window.localStorage[key] || 'null'));
            }
            else {
                deferred.reject(rejectReason)
            }
            return deferred.promise;
        }

        function remove(key) {
            var deferred = $q.defer();
            if (isSupported()) {
                delete $window.localStorage[key];
                deferred.resolve(true);
            }
            else {
                deferred.reject(rejectReason)
            }
            return deferred.promise;
        }
    });
