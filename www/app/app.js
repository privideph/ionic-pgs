
angular.module('ionic-pgs', [
        'ionic',
        'ngCordova',
        'ionic-pgs.controllers',
        'ionic-pgs.components',
        'ngMessages'
    ])

    .run(function ($ionicPlatform, AuthService, $rootScope, $state) {
        $ionicPlatform.ready(function () {
            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }
            if (window.StatusBar) {
                // org.apache.cordova.statusbar required
                StatusBar.styleDefault();
            }
        });

        $rootScope.$on('$stateChangeStart', function(event, toState, toParams){
            if(toState.data && toState.data.loginRequired && !AuthService.isAuthenticated())
            {
                event.preventDefault();
                $state.go('login');
            }
        });
    })

    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider

            .state('app', {
                url: "/app",
                abstract: true,
                templateUrl: "app/components/menu-layout/menu-layout.html",
                controller: 'MenuCtrl',
                data: {
                    loginRequired: true
                }
            })

            .state('app.users', {
                url: "/users",
                views: {
                    'menuContent': {
                        templateUrl: "app/pages/users/list/users-list.html",
                        controller: "UsersListCtrl as usersList"
                    }
                }
            })

            .state('app.users.edit', {
                url: "/:userId",
				abstract: true,
                views: {
                    'menuContent@app': {
                        templateUrl: "app/pages/users/edit/users-edit.html",
						controller: "UsersEditCtrl as usersEdit"
                    }
                },
				resolve: {
					user: function($stateParams, UsersService){
						return $stateParams.userId ?
							UsersService.getUser($stateParams.userId) :
							UsersService.newUser();
					}
				}
            })

			.state('app.users.edit.main', {
				url: "/main",
				cache: false,
				views: {
					'user-edit-main': {
						templateUrl: "app/pages/users/edit/main/users-edit-main.html",
						controller: "UsersEditMainCtrl as usersEditMain"
					}
				}
			})

			.state('app.users.edit.notes', {
				url: "/notes",
				views: {
					'user-edit-notes': {
						templateUrl: "app/pages/users/edit/notes/users-edit-notes.html",
						controller: "UsersEditNotesCtrl"
					}
				}
			})

            .state('login', {
                url: '/login',
                templateUrl: 'app/pages/login/login.html'
            });

        $urlRouterProvider.otherwise('/login');
    });
